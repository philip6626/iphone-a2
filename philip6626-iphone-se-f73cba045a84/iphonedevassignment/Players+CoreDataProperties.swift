//
//  Players+CoreDataProperties.swift
//  iphonedevassignment
//
//  Created by Philip on 19/9/17.
//  Copyright © 2017 swifty. All rights reserved.
//

import Foundation
import CoreData


extension Players {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Players> {
        return NSFetchRequest<Players>(entityName: "Players")
    }

    @NSManaged public var player_name: String?

}
