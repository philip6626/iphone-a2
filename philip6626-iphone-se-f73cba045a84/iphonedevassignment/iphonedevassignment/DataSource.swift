import Foundation

// Class for storing hard coded data
class DataSource: DataObservable {
    static let singleton: DataSource = DataSource()
    
    var tournamentList: [Tournament] = []
    var gameList: [Game] = []
    var gameDatabase = GameDatabase.sharedInstance
    private var games : Games?
    var teamList: [Team] = []
    var teamDatabase = TeamDatabase.sharedInstance
    private var teams : Teams?
    var matchList: [Match] = []
    var matchDatabase = MatchDatabase.sharedInstance
    private var match : Matches?
    var favouriteList: [Favourite] = []
    var favouriteDatabase = FavouriteDatabase.sharedInstance
    private var favourites: Favorites?
    var playerList : [Player] = []
    
    var observerList: [DataObserver] = []
    
    // Add observer
    func addObserver(observer: DataObserver) {
        observerList.append(observer)
    }
    
    
    private init() {
        gameList = [
            Game(id: 4, title: "Dota 2", logo: "dota_2_logo"),
            Game(id: 1, title: "League of Legends", logo: "league_of_legends_logo")
        ]
        //if there is somethiing in the game database
        if(!gameDatabase.isGamesEmpty()){
            gameDatabase.deleteAllGame()
            for game in gameList{
                gameDatabase.saveGames(Int64(game.id), game_logo: game.logo, game_title: game.title, existing: games)
            }
        }
        else{
            for game in gameList {
                gameDatabase.saveGames(Int64(game.id), game_logo: game.logo, game_title: game.title, existing: games)
            }
        }
    }
    
static func saveTeams(){
        
    if(!DataSource.singleton.teamDatabase.isTeamsEmpty()){
        DataSource.singleton.teamDatabase.deleteAllTeam()
        for team in DataSource.singleton.teamList{
            print(team.id)
            print("this is from db")
            DataSource.singleton.teamDatabase.saveTeams(Int64(team.id), team_logo: team.logo, team_name: team.name, existing: DataSource.singleton.teams)
        }
    }
    else{
            for team in DataSource.singleton.teamList {
                print(team.id)
                print("this is from db")
                DataSource.singleton.teamDatabase.saveTeams(Int64(team.id), team_logo: team.logo, team_name: team.name, existing: DataSource.singleton.teams)
            }
        }
    }
    
    // Auto decide add/remove favourite
    @discardableResult public func autoEditFavourite(favourite: Favourite, reminder : String) -> Bool {
        if (favouriteList.contains { $0.getUID() == favourite.getUID() }) {
            removeFavourite(favourite: favourite)
            return false
        } else {
            addFavourite(favourite: favourite, reminder: reminder)
            return true
        }
    }
    
    public func addFavourite(favourite: Favourite, reminder: String) {
        favouriteList.append(favourite)
        favouriteDatabase.saveFavourites(favourite.getUID(), favourite_name: favourite.getFavouriteName(), reminder : reminder, existing: favourites )
        updateFavourite(favouriteList.count - 1)
    }
    
    public func removeFavourite(favourite: Favourite) {
        favouriteDatabase.getFavourites()
        let index = favouriteList.index { $0.getUID() == favourite.getUID() }
        if (index != nil) {
            favouriteList.remove(at: index!)
            favouriteDatabase.deleteFavourites(favouriteDatabase.favouritedb[index!])
            favouriteDatabase.favouritedb.remove(at: index!)
            updateFavourite(index!)
        }
    }
    
    // Update observers
    private func updateFavourite(_ index: Int) {
        observerList.forEach { observer in
            observer.updateFavourite(index)
        }
    }
}
