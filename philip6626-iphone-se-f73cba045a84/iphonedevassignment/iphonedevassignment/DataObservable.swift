import Foundation

protocol DataObservable {
    func addObserver(observer: DataObserver)
}
