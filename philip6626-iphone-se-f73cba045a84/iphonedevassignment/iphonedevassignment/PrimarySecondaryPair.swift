import Foundation

// Pairs of primary and secondary view will have the same type
protocol PrimarySecondaryPair {
    func getType() -> String
}
