//
//  Player.swift
//  iphonedevassignment
//
//  Created by Philip on 25/9/17.
//  Copyright © 2017 swifty. All rights reserved.
//

import Foundation
class Player{
    
    var id: Int = 0
    var name: String = ""
    
    var logo_source: URL?
    
    init(id: Int, name: String) {
        self.id = id
        self.name = name
    }
    
    init(epsDictionary: [String : Any]) {
        
        self.id = (epsDictionary["id"] as? Int)!
        self.name = (epsDictionary["name"] as? String)!
        
        
        print("++++++++++")
        print(id)
        print(name)
        
        
        
        
        print("-----------")
        
    }
    
    
    
    
    static func getAllPlayersAPI() -> [Player]
    {
        var players = [Player]()
        
        let jsonFile = Bundle.main.path(forResource: "teamSampleAPI", ofType: "json")
        
        let jsonFileURL = URL(fileURLWithPath: jsonFile!)
        
        let jsonData = try? Data(contentsOf: jsonFileURL)
        
        if let jsonDictionary = Network.parseJSONFromData(jsonData) {
            let epsDictionaries = jsonDictionary
            
            for epsDictionary in epsDictionaries {
                let newEpisode = Player(epsDictionary: epsDictionary as! [String : Any])
                players.append(newEpisode)
            }
        }
        
        return players
    }
    
    static func getTeamPlayers(id : Int) -> [Player]
    {
        var players = [Player]()
        
        let jsonFile = Bundle.main.path(forResource: "teamSampleAPI", ofType: "json")
        
        let jsonFileURL = URL(fileURLWithPath: jsonFile!)
        
        let jsonData = try? Data(contentsOf: jsonFileURL)
        
        if let jsonDictionary = Network.parseJSONFromData(jsonData) {
            let epsDictionaries = jsonDictionary
            
            for epsDictionary in epsDictionaries {
                let newEpisode = Player(epsDictionary: epsDictionary as! [String : Any])
                if id==newEpisode.getID()
                {
                    players.append(newEpisode)
                    DataSource.singleton.playerList.append(newEpisode)
                }
            }
        }
        
        return players
    }
    
    
    
    
    
    func getID() -> Int {
        return id
    }
    
    func getFavouriteName() -> String {
        return name
    }
}
