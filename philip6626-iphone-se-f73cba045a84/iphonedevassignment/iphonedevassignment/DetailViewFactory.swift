import Foundation
import UIKit

class DetailViewFactory {
    
    // Instantiates detail view based on favourite type
    static func getDetailView(storyboard: UIStoryboard, favourite: Favourite) -> DetailViewController? {
        if (favourite is Match) {
            let view = storyboard.instantiateViewController(withIdentifier: "matchView") as! MatchViewController
            view.setMatch(match: favourite as! Match)
            return view
            
        } else if (favourite is Team) {
            let view = storyboard.instantiateViewController(withIdentifier: "teamView") as! TeamViewController
            view.setTeam(team: favourite as! Team)
            return view
        }
        return nil
    }
}
