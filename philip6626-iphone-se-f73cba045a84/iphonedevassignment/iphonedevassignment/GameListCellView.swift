import Foundation
import UIKit

class GameListCellView: UITableViewCell {
    @IBOutlet weak var gameLogoView: UIImageView!
    
    public func setGame(game: Game) {
        gameLogoView.image = UIImage(named:game.logo)
    }
    
    
}
