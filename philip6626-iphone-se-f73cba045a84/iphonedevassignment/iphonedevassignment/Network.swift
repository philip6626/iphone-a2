//
//  Network.swift
//  iphonedevassignment
//
//  Created by Sajid Pakirdeen on 21/9/17.
//  Copyright © 2017 swifty. All rights reserved.
//

import Foundation

let token:String = "&token=Mfo3sAMr1_v5NO5_5jyT9bC-TgNC8qlPTtelCuH0DUw6vcf1glA"
let endPoint:String = "https://api.pandascore.co/"

class Network
{
    
    lazy var configuration: URLSessionConfiguration = URLSessionConfiguration.default
    lazy var session: URLSession = URLSession(configuration: self.configuration)
    
    let url: URL
    
    init(url: URL) {
        self.url = url
    }
    
    typealias ImageDataHandler = ((Data) -> Void)
    
    func downloadImage(_ completion: @escaping ImageDataHandler)
    {
        let request = URLRequest(url: self.url)
        let dataTask = session.dataTask(with: request, completionHandler: { (data, response, error) in
            
            if error == nil {
                if let httpResponse = response as? HTTPURLResponse {
                    switch (httpResponse.statusCode) {
                    case 200:
                        if let data = data {
                            completion(data)
                        }
                    default:
                        print(httpResponse.statusCode)
                    }
                }
            } else {
                
                print("Error: \(error?.localizedDescription)")
            }
        })
        
        dataTask.resume()
    }
    
    //Checking Null values in Json
    func checkForNull(value:Any) -> String
    {
        if(value as! NSObject == NSNull() || value as! String == "")
        {
            return " "
        }
        else
        {
            return value as! String
        }
    }

    
    

    
    //    Customizing the get request
    static func getRequestData(_ apiLink:String, completion: @escaping ((Data)->(Void))){
        
        
        let url = URL(string: apiLink)
        
        let session = URLSession.shared
        session.dataTask(with: url!) { (data, response, error) in
            if let response = response {
            }
            
            if let data = data {
                _ = completion(data)
            }
            }.resume()
    }
    
  
    static func getTournamentByGameID(_ gameID:String, completion: @escaping ((Data)->(Void))){
        
        let preFix = "videogames/"
        let postFix = "/tournaments"
        
        let overRideURL = "https://api.pandascore.co/tournaments?token=Mfo3sAMr1_v5NO5_5jyT9bC-TgNC8qlPTtelCuH0DUw6vcf1glA"
        
        let preURL = endPoint + preFix + gameID + postFix + "?" + token
        let url = URL(string: overRideURL)
        
        let session = URLSession.shared
        session.dataTask(with: url!) { (data, response, error) in
            if let response = response {
                print(response)
            }
            
            if let data = data {
                _ = completion(data)
            }
            
            }.resume()
    }

}

// Convert the JSON Data
extension Network
{
    static func parseJSONFromData(_ jsonData: Data?) -> [AnyObject]?
    {
        if let data = jsonData {
            do {
                let jsonDictionary = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [AnyObject]
                return jsonDictionary
                
            } catch let error as NSError {
                print("error processing json data: \(error.localizedDescription)")
            }
        }
        
        return nil
    }


}
