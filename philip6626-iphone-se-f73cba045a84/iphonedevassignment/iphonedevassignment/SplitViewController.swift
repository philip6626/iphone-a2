import Foundation
import UIKit

// Overrides default split view behaviour
class SplitViewController: UISplitViewController, UISplitViewControllerDelegate {
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.preferredDisplayMode = UISplitViewControllerDisplayMode.allVisible
        
//        get Data from API Team infomraton
       func getJsonData(json: Data){
        Team.getAllTeamsAPIFromWebSwiftyJson(json)
        }
        
        Network.getRequestData("https://api.pandascore.co/teams?&token=Mfo3sAMr1_v5NO5_5jyT9bC-TgNC8qlPTtelCuH0DUw6vcf1glA", completion: getJsonData)

        
        func getGamesFromID(json: Data){
            Tournament.getTournamentFromGameID(json)
    
        }
        Network.getTournamentByGameID("01", completion: getGamesFromID)
        
        
        
    }
    
    
    // When collapsing to single view, determine if secondary view should collapse (disappear) here
    func splitViewController(
        _ splitViewController: UISplitViewController,
        collapseSecondary secondaryViewController: UIViewController,
        onto primaryViewController: UIViewController) -> Bool {
        
        // Collapse the default detail view
        if (secondaryViewController is DefaultDetailViewController) {
            return true
        }
        
        let primaryNav = primaryViewController as! UINavigationController
        let primaryTab = primaryNav.viewControllers.first as! UITabBarController
        let isPrimaryPair: Bool = primaryTab.selectedViewController is PrimarySecondaryPair
        let secondaryNav = (secondaryViewController as? UINavigationController)!
        let isSecondaryPair: Bool = secondaryNav.viewControllers.last is PrimarySecondaryPair
        
        // If on home tab with a selected detail tab dont collapse
        if (primaryTab.selectedIndex == 0 && isSecondaryPair) {
            return false
        }
        
        // If both views are pairs
        if (isPrimaryPair && isSecondaryPair) {
            let primaryPair: PrimarySecondaryPair = primaryTab.selectedViewController as! PrimarySecondaryPair
            let secondaryPair: PrimarySecondaryPair = secondaryNav.viewControllers.last as! PrimarySecondaryPair
            // If both views are the same pair don't collapse the detail view
            if (primaryPair.getType() == secondaryPair.getType()) {
                return false
            }
            // Otherwise collapse
            else {
                return true
            }
        }
        // Collapse detail view for all other cases
        else {
            return true
        }
    }
    
    // When expanding to split view, set secondary view here
    func splitViewController(_ splitViewController: UISplitViewController,
                             separateSecondaryFrom primaryViewController: UIViewController) -> UIViewController? {
        
        let navigation: UINavigationController = splitViewController.viewControllers.first as! UINavigationController
        let isDetailView = navigation.viewControllers.last is DetailViewController
        var isNavigationDetailView = false
        if (navigation.viewControllers.last is UINavigationController) {
            isNavigationDetailView = (navigation.viewControllers.last as! UINavigationController).viewControllers.last is DetailViewController
        }
        
        // Set detail view as secondary view when avaiable
        if (isDetailView || isNavigationDetailView) {
            return navigation.viewControllers.last
        // Otherwise use the default detail view
        } else {
            return storyboard?.instantiateViewController(withIdentifier: "defaultDetailView")
        }
    }
 
    
}
