//
//  TournamentDatabase.swift
//  iphonedevassignment
//
//  Created by Philip on 19/9/17.
//  Copyright © 2017 swifty. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class TournamentDatabase
{
    static let sharedInstance = TournamentDatabase()
    private init()
    {
        managedContext = appDelegate.persistentContainer.viewContext
    }
    
    // Get a reference to your App Delegate
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    // Hold a reference to the managed context
    let managedContext: NSManagedObjectContext
    
    var tournamentdb = [Tournaments]()
    
    func getTournaments(_ indexPath:IndexPath)->Tournaments
    {
        return tournamentdb[indexPath.row]
    }
    
    //MARK : - CRUD
    
    func saveTournaments(_ tournament_id:int_fast64_t, tournament_name:String,tournament_logo:String, tournament_winner_id:int_fast64_t, existing: Tournaments? )
    {
        // Create a new managed object and insert it into the context, so it can be saved
        // into the database
        let entity =  NSEntityDescription.entity(forEntityName: "Tournaments",
                                                 in:managedContext)
        
        // Update the existing object with the data passed in from the View Controller
        if let _ = existing
        {
            existing!.tournament_id = tournament_id
            existing!.tournament_name = tournament_name
            existing!.tournamet_logo = tournament_logo
            existing!.tournament_winner_id = tournament_winner_id
        }
            
            // Create a new movie object and update it with the data passed in from the View Controller
        else
        {
            // Create an object based on the Entity
            let tournaments = Tournaments(entity: entity!, insertInto: managedContext)
            tournaments.tournament_id = tournament_id
            tournaments.tournament_name = tournament_name
            tournaments.tournamet_logo = tournament_logo
            tournaments.tournament_winner_id = tournament_winner_id
        }
        updateTournaments()
    }
    
    func getTournaments()
    {
        do
        {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName:"Tournaments")
            
            let results = try(managedContext.fetch(fetchRequest))
            tournamentdb = results as! [Tournaments]
        }
        catch let error as NSError
        {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    func isTournamentEmpty() -> Bool{
        do
        {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName:"Tournaments")
            
            let results = try(managedContext.fetch(fetchRequest))
            tournamentdb = results as! [Tournaments]
            if tournamentdb.count == 0 {
                return true
            }
            else{
                return false
            }
        }
        catch let error as NSError
        {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        return false
    }
    
    func deleteAllTournament(){
        do{
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName:"Tournaments")
            
            let results = try(managedContext.fetch(fetchRequest))
            tournamentdb = results as! [Tournaments]
            
            for tournament in tournamentdb{
                managedContext.delete(tournament)
            }
            updateTournaments()
        }
        catch let error as NSError
        {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        
    }
    
    func deleteTournaments(_ tournament:Tournaments)
    {
        managedContext.delete(tournament)
        updateTournaments()
    }
    
    func updateTournaments()
    {
        do
        {
            try managedContext.save()
        }
        catch let error as NSError
        {
            print("Could not save\(error), into tournaments database, \(error.userInfo)")
        }
    }

}
