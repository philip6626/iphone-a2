import Foundation
import SwiftyJSON


class Tournament: Favourite, Filter {
    
    var id:Int = 0
    var name:String = ""
    var logo:String = ""
    var winner:Team?
    var game: Game?
    var matchList:[Match] = []
    var teamList:[Team] = []
    var UID: String = ""
    
    init(id: Int, name: String, logo: String, game: Game, matchList: [Match], teamList: [Team], winner: Team) {
        self.id = id
        self.name = name
        self.logo = logo
        self.game = game
        self.matchList = matchList
        self.teamList = teamList
        self.winner = winner
        self.UID = UUID().uuidString
    }
    
    init(tournDictionary: [String : Any]) {
        
        self.id = (tournDictionary["id"] as? Int)!
        self.name = (tournDictionary["name"] as? String)!
        
        if tournDictionary["winner_id"] as? Int == nil {
            print("winner Id is null")
            self.winner = Team(id: 999, name: "", logo: "", playerList: ["Test","Test"])
        }else{
            self.winner = Team(id: (tournDictionary["winner_id"] as? Int)!, name: "", logo: "", playerList: ["Test","Test"])
        }
        
        
    }
    
    
    func setWinnerId(team: Team) {
        self.winner = team
    }
    
    func getUID() -> String {
        return UID
    }
    
    func getFavouriteName() -> String {
        return name
    }
    
    
    static func getTournamentFromGameID(_ jsonData:Data)
    {
        var count=0;
        let json = JSON(data:jsonData)
        
        let tournments = json.arrayValue
        print(tournments[0])
        
        for tourn in tournments {
            
            let newTournment = Tournament(tournDictionary: tourn.dictionaryObject!)
            print("This is Tournament")
            print(newTournment.id)
            print(newTournment.name)
            
            let gameDics = json[count]["videogame"].dictionaryObject
            newTournment.game = Game(gameDictionary: gameDics!)
            print("Current Game \(String(describing: newTournment.game?.id))")
            
            let teamDics = json[count]["teams"].arrayValue
            for team in teamDics {
                let addTeam = Team(teamDictionary:team.dictionaryObject!)
                newTournment.teamList.append(addTeam)
            }
            
            let matchDic = json[count]["matches"].arrayValue
            
            var teamA:Team?
            var teamB:Team?
            //getting the current date
            var localTimeZoneAbbreviation: String { return  NSTimeZone.local.abbreviation(for: Date())! }
            let currentDate = Date()
            var innerCount = 0;
            for team in matchDic {
                
                if innerCount==0
                {
                    teamA = Team(teamDictionary:team.dictionaryObject!)
                    innerCount=innerCount+1
                }else{
                    teamB = Team(teamDictionary:team.dictionaryObject!)
                }
            }
            if teamA == nil{
                teamA = Team(id: 999, name: "No Team", logo: "NoString", playerList: ["noTeam1","Noteam2"])
            }
            if teamB == nil{
                teamB = Team(id: 999, name: "No Team", logo: "NoString", playerList: ["noTeam1","Noteam2"])
            }
            
            // Getting MatchList
            let match = Match(id: 111, teamA: teamA!, teamB: teamB!, date: currentDate, teamAScore: 100, teamBScore: 100, winner:newTournment.winner! )
            print("------Match-------")
            
            newTournment.matchList.append(match)
            
            
            DataSource.singleton.tournamentList.append(newTournment)
            count=count+1;
        }
        
        
        
    }
    // testing all tournment data
    static func printAllTournamentData(){
        
        
        for tournanment in DataSource.singleton.tournamentList {
            print("All tournaments ----------- ")
            print(tournanment.id)
            print(tournanment.name)
            for match in tournanment.matchList {
                print("teamA \(match.teamA.name)/")
                print("teamB \(match.teamB.name)/")
            }
            for team in tournanment.teamList{
                print("My Teams \(team.id)/")
                print("My Teams \(team.id)/")
            }
        }
        
    }
    
    
    
    
    
}


