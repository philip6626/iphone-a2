//
//  MatchDatabase.swift
//  iphonedevassignment
//
//  Created by Philip on 19/9/17.
//  Copyright © 2017 swifty. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class MatchDatabase
{
    static let sharedInstance =  MatchDatabase()
    private init()
    {
        managedContext = appDelegate.persistentContainer.viewContext
    }
    
    // Get a reference to your App Delegate
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    // Hold a reference to the managed context
    let managedContext: NSManagedObjectContext
    
    var matchdb = [Matches]()
    
    func getMatches(_ indexPath:IndexPath)->Matches
    {
        return matchdb[indexPath.row]
    }
    
    //MARK : - CRUD
    
    func saveMatches(_ match_id:int_fast64_t, date:Date,team_a:int_fast64_t, team_a_score:int_fast64_t, team_b:int_fast64_t, team_b_score:int_fast64_t, match_winner_id:int_fast64_t, existing: Matches? )
    {
        // Create a new managed object and insert it into the context, so it can be saved
        // into the database
        let entity =  NSEntityDescription.entity(forEntityName: "Matches",
                                                 in:managedContext)
        
        // Update the existing object with the data passed in from the View Controller
        if let _ = existing
        {
            existing!.match_id = match_id
            existing!.date = date as NSDate
            existing!.team_a = team_a
            existing!.team_a_score = team_a_score
            existing!.team_b = team_b
            existing!.team_b_score = team_b_score
            existing!.match_winner_id = match_winner_id
        }
            
            // Create a new movie object and update it with the data passed in from the View Controller
        else
        {
            // Create an object based on the Entity
            let matches = Matches(entity: entity!, insertInto: managedContext)
            matches.match_id = match_id
            matches.date = date as NSDate
            matches.team_a = team_a
            matches.team_a_score = team_a_score
            matches.team_b = team_b
            matches.team_b_score = team_b_score
            matches.match_winner_id = match_winner_id
        }
        updateMatches()
    }
    
    func getMatches()
    {
        do
        {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName:"Matches")
            
            let results = try(managedContext.fetch(fetchRequest))
            matchdb = results as! [Matches]
        }
        catch let error as NSError
        {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    func isMatchEmpty() -> Bool{
        do
        {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName:"Matches")
            
            let results = try(managedContext.fetch(fetchRequest))
            matchdb = results as! [Matches]
            if matchdb.count == 0 {
                return true
            }
            else{
                return false
            }
        }
        catch let error as NSError
        {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        return false
    }
    
    func deleteAllMatch(){
        do{
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName:"Matches")
            
            let results = try(managedContext.fetch(fetchRequest))
            matchdb = results as! [Matches]
            
            for match in matchdb{
                managedContext.delete(match)
            }
            updateMatches()
        }
        catch let error as NSError
        {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        
    }
    
    func deleteMatches(_ match:Matches)
    {
        managedContext.delete(match)
        updateMatches()
    }
    
    func updateMatches()
    {
        do
        {
            try managedContext.save()
        }
        catch let error as NSError
        {
            print("Could not save\(error), into matches database, \(error.userInfo)")
        }
    }

}
