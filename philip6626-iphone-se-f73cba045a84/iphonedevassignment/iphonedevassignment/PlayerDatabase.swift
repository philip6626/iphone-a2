//
//  PlayerDatabase.swift
//  iphonedevassignment
//
//  Created by Philip on 19/9/17.
//  Copyright © 2017 swifty. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class PlayerDatabase
{
    static let sharedInstance = PlayerDatabase()
    private init()
    {
        managedContext = appDelegate.persistentContainer.viewContext
    }
    
    // Get a reference to your App Delegate
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    // Hold a reference to the managed context
    let managedContext: NSManagedObjectContext
    
    var playerdb = [Players]()
    
    func getPlayers(_ indexPath:IndexPath)->Players
    {
        return playerdb[indexPath.row]
    }
    
    //MARK : - CRUD
    
    func savePlayers(_ player_name:String, existing: Players? )
    {
        // Create a new managed object and insert it into the context, so it can be saved
        // into the database
        let entity =  NSEntityDescription.entity(forEntityName: "Players",
                                                 in:managedContext)
        
        // Update the existing object with the data passed in from the View Controller
        if let _ = existing
        {
            existing!.player_name = player_name
        }
            
            // Create a new movie object and update it with the data passed in from the View Controller
        else
        {
            // Create an object based on the Entity
            let players = Players(entity: entity!, insertInto: managedContext)
            players.player_name = player_name
        }
        updatePlayers()
    }
    
    func getPlayers()
    {
        do
        {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName:"Players")
            
            let results = try(managedContext.fetch(fetchRequest))
            playerdb = results as! [Players]
        }
        catch let error as NSError
        {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    func isPlayersEmpty() -> Bool{
        do
        {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName:"Players")
            
            let results = try(managedContext.fetch(fetchRequest))
            playerdb = results as! [Players]
            if playerdb.count == 0 {
                return true
            }
            else{
                return false
            }
        }
        catch let error as NSError
        {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        return false
    }
    
    func deleteAllPlayer(){
        do{
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName:"Player")
            
            let results = try(managedContext.fetch(fetchRequest))
            playerdb = results as! [Players]
            
            for player in playerdb{
                managedContext.delete(player)
            }
            updatePlayers()
        }
        catch let error as NSError
        {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        
    }
    
    func deletePlayers(_ player:Players)
    {
        managedContext.delete(player)
        updatePlayers()
    }
    
    func updatePlayers()
    {
        do
        {
            try managedContext.save()
        }
        catch let error as NSError
        {
            print("Could not save\(error), into players database, \(error.userInfo)")
        }
    }
    
}

