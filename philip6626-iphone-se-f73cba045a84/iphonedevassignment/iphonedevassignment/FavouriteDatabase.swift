//
//  FavouriteDatabase.swift
//  iphonedevassignment
//
//  Created by Philip on 5/9/17.
//  Copyright © 2017 swifty. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class FavouriteDatabase
{
    static let sharedInstance = FavouriteDatabase()
    private init()
    {
        managedContext = appDelegate.persistentContainer.viewContext
    }
    
    // Get a reference to your App Delegate
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    // Hold a reference to the managed context
    let managedContext: NSManagedObjectContext
    
    var favouritedb = [Favorites]()
    
    func getFavourites(_ indexPath:IndexPath)->Favorites
    {
        return favouritedb[indexPath.row]
    }
    
    
    //MARK : - CRUD
    
    func saveFavourites(_ favourite_id:String, favourite_name:String, reminder:String, existing: Favorites? )
    {
        // Create a new managed object and insert it into the context, so it can be saved
        // into the database
        let entity =  NSEntityDescription.entity(forEntityName: "Favourites",
                                                 in:managedContext)
        
        // Update the existing object with the data passed in from the View Controller
        if let _ = existing
        {
            existing!.favourite_id = favourite_id
            existing!.favourite_name = favourite_name
            existing!.reminder = reminder
        }
        
        // Create a new movie object and update it with the data passed in from the View Controller
        else
        {
            // Create an object based on the Entity
            let favourites = Favorites(entity: entity!, insertInto: managedContext)
            favourites.favourite_id = favourite_id
            favourites.favourite_name = favourite_name
            favourites.reminder = reminder
        }
        updateFavourites()
    }
    
    func getFavourites()
    {
        do
        {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName:"Favourites")
            
            let results = try(managedContext.fetch(fetchRequest))
            favouritedb = results as! [Favorites]
        }
        catch let error as NSError
        {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    func deleteFavourites(_ favourite:Favorites)
    {
        managedContext.delete(favourite)
        updateFavourites()
    }
    
    func updateFavourites()
    {
        do
        {
            try managedContext.save()
        }
        catch let error as NSError
        {
            print("Could not save\(error), into favourites database, \(error.userInfo)")
        }
    }
    
}
