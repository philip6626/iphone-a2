import Foundation
import SwiftyJSON

class Team: Favourite, Filter{
    
    var id: Int
    var name: String
    var logo: String
    var playerList: [String] = []
    var UID: String
    
    var logo_source: URL?
    
    init(id: Int, name: String, logo: String, playerList: [String]) {
        self.id = id
        self.name = name
        self.logo = logo
        self.playerList = playerList
        self.UID = UUID().uuidString
    }
    
    
    init(teamDictionary: [String : Any]) {
        
    self.id = (teamDictionary["id"] as? Int)!
        
    self.name = (teamDictionary["name"] as? String)!
        
    if teamDictionary["image_url"] as? String == nil {
        self.logo = "http://3.bp.blogspot.com/-WW7hY9_mpWY/Uw4R_GdzL8I/AAAAAAAAAVc/m8xHpeGrM9c/s1600/1393452711_graph.png"
        self.logo_source = URL(string: "https://png.icons8.com/remove-image/Dusk_Wired/64")
    }else{
        self.logo = (teamDictionary["image_url"] as? String)!
        self.logo_source = URL(string: teamDictionary["image_url"] as! String)
    }
        self.UID = UUID().uuidString
        
    }
    
    static func getAllTeamsAPIFromWebSwiftyJson(_ jsonData:Data)
    {
        
        let teamJson = JSON(data:jsonData)
      
        let teamDictionaries = teamJson.arrayValue
        
        for teamDic in teamDictionaries {
            
            let newEpisode = Team(teamDictionary: teamDic.dictionaryObject!)
            
            DataSource.singleton.teamList.append(newEpisode)
        }
        
        DataSource.saveTeams()
    }
    

    func getUID() -> String {
        return UID
    }
    
    func getFavouriteName() -> String {
        return name
    }
}
