import Foundation
import UIKit

class TournamentListViewController : UITableViewController, FilteredView {
    private let tournamentSegue: String = "tournamentSegue"
    private var tournamentList: [Tournament] = DataSource.singleton.tournamentList
    private var tournamentFilter: Tournament!
    private var filtered = false
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.navigationItem.title = "Tournaments"
        self.tabBarController?.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Show All", style: UIBarButtonItemStyle.plain, target: self, action: #selector(unfilter))
        self.tabBarController?.navigationItem.leftBarButtonItem?.isEnabled = filtered
        self.tableView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tournamentList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let tournamentCell:TournamentListCellView = self.tableView.dequeueReusableCell(withIdentifier: "tournamentCell") as! TournamentListCellView
        
        tournamentCell.setTournament(tournament: tournamentList[indexPath.row])
        
        return tournamentCell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tournamentFilter = tournamentList[indexPath.row]
        print(tournamentList[indexPath.row])
        tabBarController?.selectedIndex = 3
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == tournamentSegue) {
            let viewController: MatchListViewController = segue.destination as! MatchListViewController
            viewController.filter(tournament: tournamentFilter)
        }
    }
    
    // Filter tournament list by game
    func filter(game: Game) {
        tournamentList = DataSource.singleton.tournamentList
        tournamentList = tournamentList.filter { $0.game?.id == game.id }
        tableView.reloadData()
        
        for tournanment in tournamentList {
            print("All tournaments ----------- ")
            print(tournanment.id)
            print(tournanment.name)
            for match in tournanment.matchList {
                print("teamA \(match.teamA.name)/")
                print("teamB \(match.teamB.name)/")
            }
            for team in tournanment.teamList{
                print("My Teams \(team.id)/")
                print("My Teams \(team.name)/")
            }
        }
        
        self.tabBarController?.navigationItem.leftBarButtonItem?.isEnabled = true
        filtered = true
    }
    
    // Filter tournament list by team
    func fitler(team: Team) {
        tournamentList = DataSource.singleton.tournamentList
        tournamentList = tournamentList.filter() {
            for participant in $0.teamList {
                if (participant.id == team.id) {
                    return true
                }
            }
            return false
        }
        filtered = true
    }
    
    func unfilter() {
        if (!filtered) {
            return
        }
        tournamentList = DataSource.singleton.tournamentList
        
        UIView.animate(withDuration: 0.3) {
            self.tableView.visibleCells.forEach { cell in
                cell.contentView.alpha = 0
            }
        }
        
        tableView.reloadData()
        
        self.tableView.visibleCells.forEach { cell in
            cell.contentView.alpha = 0
        }
        UIView.animate(withDuration: 0.3) {
            self.tableView.visibleCells.forEach { cell in
                cell.contentView.alpha = 1
            }
        }
        
        self.tabBarController?.navigationItem.leftBarButtonItem?.isEnabled = false
        filtered = false
    }

    
}
