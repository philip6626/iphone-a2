import Foundation
import UIKit

class TournamentListCellView: UITableViewCell {
    @IBOutlet weak var tournamentTitleView: UILabel!
    
    public func setTournament(tournament: Tournament) {
        tournamentTitleView.text = tournament.name
    }
}
