import Foundation
import SwiftyJSON

class Game: Favourite, Filter {
    var id: Int = 0
    var title: String = ""
    var logo: String = ""
    var UID: String = ""
    
    init(id: Int, title:String, logo:String) {
        self.id = id
        self.title = title
        self.logo = logo
        self.UID = UUID().uuidString
    }
    
    func getUID() -> String {
        return UID
    }
    
    func getFavouriteName() -> String {
        return title
    }
    
    
    init(gameDictionary: [String : Any]) {
        
        self.id = (gameDictionary["id"] as? Int)!
        self.title = (gameDictionary["name"] as? String)!
        
        self.logo = (gameDictionary["name"] as? String)!
        self.UID = UUID().uuidString
    }
}
