//
//  TabBarController.swift
//  iphonedevassignment
//
//  Created by Philip on 23/8/17.
//  Copyright © 2017 swifty. All rights reserved.
//

import Foundation
import UIKit

class TabBarController : UITabBarController, UITabBarControllerDelegate{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
    }
    
    func tabBarController(_ tabBarController: UITabBarController,
                          animationControllerForTransitionFrom fromVC: UIViewController,
                          to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        // Overrides all transition behaviour
        customTransition(fromView: fromVC.view, toView: toVC.view)
        return nil
    }
    
    func customTransition(fromView: UIView, toView: UIView) {
        if fromView == toView {
            return
        }
        UIView.transition(from: fromView, to: toView, duration: 0.3, options: UIViewAnimationOptions.transitionCrossDissolve) { (finished:Bool) in }
    }

}
