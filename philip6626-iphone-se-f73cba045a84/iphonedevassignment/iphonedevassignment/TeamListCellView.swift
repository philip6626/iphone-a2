import Foundation
import UIKit

class TeamListCellView : UITableViewCell {
    
    
    var team: Team!
    
    func updateUI()
    {
         teamNameView.text = team.name
        
        if let thumbnailURL = team.logo_source {
            let networkService = Network(url: thumbnailURL)
            networkService.downloadImage({ (imageData) in
                let image = UIImage(data: imageData as Data)
                DispatchQueue.main.async(execute: {
                    self.teamLogoView.image = image
                })
            })
        }
    }
    
    @IBOutlet weak var teamLogoView: UIImageView!
    @IBOutlet weak var teamNameView: UILabel!
    
    public func setTeam(team : Team){
        
        self.team = team
        self.updateUI()
    }
    
}
