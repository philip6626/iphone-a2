//
//  TeamDatabase.swift
//  iphonedevassignment
//
//  Created by Philip on 19/9/17.
//  Copyright © 2017 swifty. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class TeamDatabase
{
    static let sharedInstance = TeamDatabase()

    
    private init()
    {
        managedContext = appDelegate.persistentContainer.viewContext
    }
    
    // Get a reference to your App Delegate
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    // Hold a reference to the managed context
    let managedContext: NSManagedObjectContext
    
    var teamdb = [Teams]()
    
    
    func getTeams(_ indexPath:IndexPath)->Teams
    {
        return teamdb[indexPath.row]
    }
    
    //MARK : - CRUD
    
    func saveTeams(_ team_id:int_fast64_t, team_logo:String, team_name:String, existing: Teams? )
    {
        // Create a new managed object and insert it into the context, so it can be saved
        // into the database
        let entity =  NSEntityDescription.entity(forEntityName: "Teams",
                                                 in:managedContext)
        
        // Update the existing object with the data passed in from the View Controller
        if let _ = existing
        {
            existing!.team_id = team_id
            existing!.team_logo = team_logo
            existing!.team_name = team_name
        }
            
            // Create a new movie object and update it with the data passed in from the View Controller
        else
        {
            // Create an object based on the Entity
            let teams = Teams(entity: entity!, insertInto: managedContext)
            teams.team_id = team_id
            teams.team_logo = team_logo
            teams.team_name = team_name
        }
        updateTeams()
    }
    
    func getTeams()
    {
        do
        {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName:"Teams")
            
            let results = try(managedContext.fetch(fetchRequest))
            teamdb = results as! [Teams]
        }
        catch let error as NSError
        {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    func isTeamsEmpty() -> Bool{
        do
        {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName:"Teams")
            
            let results = try(managedContext.fetch(fetchRequest))
            teamdb = results as! [Teams]
            if teamdb.count == 0 {
                return true
            }
            else{
                return false
            }
        }
        catch let error as NSError
        {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        return false
    }
    
    func deleteTeams(_ team: Teams)
    {
        managedContext.delete(team)
        updateTeams()
    }
    
    func deleteAllTeam(){
        do{
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName:"Teams")
            
            let results = try(managedContext.fetch(fetchRequest))
            teamdb = results as! [Teams]

            for team in teamdb{
                managedContext.delete(team)
            }
            updateTeams()
        }
        catch let error as NSError
        {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        
    }
    
    func updateTeams()
    {
        do
        {
            try managedContext.save()
        }
        catch let error as NSError
        {
            print("Could not save\(error), into teams database, \(error.userInfo)")
        }
    }
    
}
