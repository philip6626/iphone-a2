import Foundation

protocol Favourite {
    func getUID() -> String // Unique id for favourite
    func getFavouriteName() -> String
}
