import Foundation

protocol DataObserver {
    func updateFavourite(_ index: Int)
}
