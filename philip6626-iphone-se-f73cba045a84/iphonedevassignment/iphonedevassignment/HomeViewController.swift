import Foundation
import UIKit

class HomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, DataObserver {
    @IBOutlet weak var favouriteTableView: UITableView!
    
    var favouriteDatabase:FavouriteDatabase = FavouriteDatabase.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DataSource.singleton.addObserver(observer: self)
        favouriteTableView.delegate = self
        favouriteTableView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.navigationItem.title = "SwiftY eSports"
        favouriteDatabase.getFavourites()
        favouriteTableView.reloadData()
        self.tabBarController?.navigationItem.leftBarButtonItem = nil
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return favouriteDatabase.favouritedb.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "favouriteCell", for: indexPath) as UITableViewCell
        let favourites = favouriteDatabase.getFavourites(indexPath)
        
        if(favourites.reminder != ""){
            cell.textLabel!.text = favourites.reminder
        }
        else{
            cell.textLabel!.text = favourites.favourite_name
        }
        
        cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        return cell
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (DataSource.singleton.favouriteList.count == 0) {
            return
        }
        
        let favourite: Favourite = DataSource.singleton.favouriteList[indexPath.row]
        
        // Set detail view based on favourite
        let view = DetailViewFactory.getDetailView(storyboard: storyboard!, favourite: favourite)
        let navigation = UINavigationController()
        navigation.pushViewController(view!, animated: true)
        splitViewController?.showDetailViewController(navigation, sender: self)
    }
    
    // Update favourite table with data and size
    func updateFavourite(_ index: Int) {
        favouriteTableView.reloadSections(IndexSet(integer: 0), with: UITableViewRowAnimation.automatic)
        //favouriteTableView.reloadRows(at: [IndexPath(item: index, section: 0)], with: UITableViewRowAnimation.automatic)
    }
    
    // MARK: - Enable Swipe to Delete
    // System method that enables swipe to delete on a row in a tableview controller.
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    
    // System method that gets called when delete is selected
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath)
    {
        favouriteDatabase.deleteFavourites(favouriteDatabase.favouritedb[indexPath.row])
        favouriteDatabase.favouritedb.remove(at: indexPath.row)
        if(DataSource.singleton.favouriteList.isEmpty != true)
        {
            do{
                DataSource.singleton.favouriteList.remove(at: indexPath.row)
            }

        }
        favouriteTableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.fade)
    }
    
}
