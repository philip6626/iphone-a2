import Foundation

// Represents any view that can have it's data filtered/unfiltered
protocol FilteredView {
    // Requests the view to unfilter itself
    func unfilter()
}
