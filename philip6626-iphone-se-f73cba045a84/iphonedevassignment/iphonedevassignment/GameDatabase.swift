//
//  GameDatabase.swift
//  iphonedevassignment
//
//  Created by Philip on 19/9/17.
//  Copyright © 2017 swifty. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class GameDatabase
{
    static let sharedInstance = GameDatabase()
    private init()
    {
        managedContext = appDelegate.persistentContainer.viewContext
    }
    
    // Get a reference to your App Delegate
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    // Hold a reference to the managed context
    let managedContext: NSManagedObjectContext
    
    var gamedb = [Games]()
    
    func getGames(_ indexPath:IndexPath)->Games
    {
        return gamedb[indexPath.row]
    }
    
    //MARK : - CRUD
    
    func saveGames(_ game_id:int_fast64_t, game_logo:String, game_title:String, existing: Games? )
    {
        // Create a new managed object and insert it into the context, so it can be saved
        // into the database
        let entity =  NSEntityDescription.entity(forEntityName: "Games",
                                                 in:managedContext)
        
        // Update the existing object with the data passed in from the View Controller
        if let _ = existing
        {
            existing!.game_id = game_id
            existing!.game_logo = game_logo
            existing!.game_title = game_title
        }
            
            // Create a new movie object and update it with the data passed in from the View Controller
        else
        {
            // Create an object based on the Entity
            let games = Games(entity: entity!, insertInto: managedContext)
            games.game_id = game_id
            games.game_logo = game_logo
            games.game_title = game_title
            
        }
        updateGames()
    }
    
    func getGames()
    {
        do
        {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName:"Games")
            
            let results = try(managedContext.fetch(fetchRequest))
            gamedb = results as! [Games]
        }
        catch let error as NSError
        {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    func isGamesEmpty() -> Bool{
        do
        {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName:"Games")
            
            let results = try(managedContext.fetch(fetchRequest))
            gamedb = results as! [Games]
            if gamedb.count == 0 {
                return true
            }
            else{
                return false
            }
        }
        catch let error as NSError
        {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        return false
    }
    
    func deleteAllGame(){
        do{
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName:"Games")
            
            let results = try(managedContext.fetch(fetchRequest))
            gamedb = results as! [Games]
            
            for game in gamedb{
                managedContext.delete(game)
            }
            updateGames()
        }
        catch let error as NSError
        {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        
    }
    
    func deleteGames(_ game:Games)
    {
        managedContext.delete(game)
        updateGames()
    }
    
    func updateGames()
    {
        do
        {
            try managedContext.save()
        }
        catch let error as NSError
        {
            print("Could not save\(error), into games database, \(error.userInfo)")
        }
    }
    
}
