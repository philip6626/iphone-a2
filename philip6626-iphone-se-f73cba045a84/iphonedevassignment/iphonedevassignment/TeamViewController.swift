import Foundation
import UIKit

class TeamViewController: DetailViewController, PrimarySecondaryPair, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var teamNameView: UILabel!
    @IBOutlet weak var teamLogoView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bookmarkButton: UIBarButtonItem!
    @IBOutlet weak var reminder: UITextField!
    
    private let type: String = "team"
    private var team: Team!
    private var favourites: Favorites?
    private var favouriteDatabase = FavouriteDatabase.sharedInstance
    
    // Add this team as favourite
    @IBAction func addFavourite() {
        if (team != nil) {
            DataSource.singleton.autoEditFavourite(favourite: team, reminder : reminder.text!)
        }
        updateFavourite()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if (team != nil) {
            teamNameView.text = team.name
            displayImageUrl()
            tableView.delegate = self
            tableView.dataSource = self
            
            // update table view
            self.tableView.reloadData()
        }
        updateFavourite()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return team.playerList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let playerCell:TeamPlayerListCellView = tableView.dequeueReusableCell(withIdentifier: "playerCell") as! TeamPlayerListCellView
        
        playerCell.setPlayer(player: team.playerList[indexPath.row])
        return playerCell
    }
    
    func displayImageUrl() {
        if let thumbnailURL = team.logo_source {
            let networkService = Network(url: thumbnailURL)
            networkService.downloadImage({ (imageData) in
                let image = UIImage(data: imageData as Data)
                DispatchQueue.main.async(execute: {
                    self.teamLogoView.image = image
                })
            })
        }
    }
    
    func getType() -> String {
        return type
    }
    
    // Set team for this view
    public func setTeam(team: Team){
        self.team = team
    }
    
    func updateFavourite() {
        let added = DataSource.singleton.favouriteList.filter { $0.getUID() == team.getUID() }.count > 0
        bookmarkButton.title =  added ? "Remove" : "Add"
    }
}
