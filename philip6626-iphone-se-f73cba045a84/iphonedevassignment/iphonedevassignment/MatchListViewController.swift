import Foundation
import UIKit

class MatchListViewController: UITableViewController, PrimarySecondaryPair, FilteredView {
    private let type: String = "match"
    private let matchSegue: String = "matchSegue"
    
    private var matchList: [Match] = DataSource.singleton.matchList
    private var matchFilter: Match!
    private var filtered = false
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.navigationItem.title = "Matches"
        self.tabBarController?.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Show All", style: UIBarButtonItemStyle.plain, target: self, action: #selector(unfilter))
        self.tabBarController?.navigationItem.leftBarButtonItem?.isEnabled = filtered
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return matchList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let matchCell:MatchListCellView = self.tableView.dequeueReusableCell(withIdentifier: "matchCell") as! MatchListCellView
        return matchCell
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        matchFilter = matchList[indexPath.row]
        performSegue(withIdentifier: matchSegue, sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == matchSegue) {
            var detailView: MatchViewController!
            if (segue.destination is UINavigationController) {
                detailView = (segue.destination as! UINavigationController).viewControllers.first as! MatchViewController
            } else if (segue.destination is MatchViewController) {
                detailView = segue.destination as! MatchViewController
            }
            detailView.setMatch(match: matchFilter)
        }
    }
    
    func getType() -> String {
        return type
    }
    
    public func filter(tournament: Tournament) {
        matchList = tournament.matchList
        tableView.reloadData()
        self.tabBarController?.navigationItem.leftBarButtonItem?.isEnabled = true
        filtered = true
    }
    
    func unfilter() {
        if (!filtered) {
            return
        }
        
        matchList = DataSource.singleton.matchList
        
        UIView.animate(withDuration: 0.3) {
            self.tableView.visibleCells.forEach { cell in
                cell.contentView.alpha = 0
            }
        }
        
        tableView.reloadData()
        
        self.tableView.visibleCells.forEach { cell in
            cell.contentView.alpha = 0
        }
        UIView.animate(withDuration: 0.3) {
            self.tableView.visibleCells.forEach { cell in
                cell.contentView.alpha = 1
            }
        }
        
        self.tabBarController?.navigationItem.leftBarButtonItem?.isEnabled = false
        filtered = false
    }

}


