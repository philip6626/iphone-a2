import Foundation
import UIKit

class TeamListViewController : UITableViewController, PrimarySecondaryPair {
    private let teamSegue: String = "teamSegue"
    private let type: String = "team"
    private var teamFilter : Team!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.navigationItem.title = "Teams"
        self.tabBarController?.navigationItem.leftBarButtonItem = nil
        self.tableView.reloadData()
    }

    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return DataSource.singleton.teamList.count
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let teamCell:TeamListCellView = self.tableView.dequeueReusableCell(withIdentifier: "teamCell") as! TeamListCellView
        
        teamCell.setTeam(team: DataSource.singleton.teamList[indexPath.row])
        
        return teamCell    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        teamFilter = DataSource.singleton.teamList[indexPath.row] // Set game as filter
        performSegue(withIdentifier: teamSegue, sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == teamSegue) {
            var viewController: TeamViewController!
            
            if (segue.destination is UINavigationController) {
                viewController = (segue.destination as! UINavigationController).viewControllers.first as! TeamViewController
            } else if (segue.destination is TeamViewController) {
                viewController = segue.destination as! TeamViewController
            }
            viewController?.setTeam(team: teamFilter) // Apply filter to view
        }
    }

    func getType() -> String {
        return type
    }
    
}
