import Foundation
import UIKit

class GameListViewController: UITableViewController {
    @IBOutlet weak var favouriteButton: UIBarButtonItem?
    
    private let gameSegue: String = "gameSegue"
    private var gameFilter: Game!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.navigationItem.title = "Games"
        self.tabBarController?.navigationItem.leftBarButtonItem = nil
        self.tableView.reloadData()

    }
    

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataSource.singleton.gameList.count
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let gameCell:GameListCellView = self.tableView.dequeueReusableCell(withIdentifier: "gameCell") as! GameListCellView
        
        gameCell.setGame(game: DataSource.singleton.gameList[indexPath.row])
        
        return gameCell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        gameFilter = DataSource.singleton.gameList[indexPath.row] /// Set game at row as filter
        
        
        tabBarController?.selectedIndex = 2
        (tabBarController?.viewControllers?[2] as! TournamentListViewController).filter(game: gameFilter)
    
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == gameSegue) {
            let viewController: TournamentListViewController = segue.destination as! TournamentListViewController
            viewController.filter(game: gameFilter) // Apply game as filter to destination view
        }
    }
    
    
    
}
