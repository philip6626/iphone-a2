import Foundation

class Match: Favourite {
    
    var id: Int = 0
    var teamA: Team
    var teamB: Team
    var date: Date = Date()
    var teamAScore: Int = 0
    var teamBScore: Int = 0
    var winner: Team
    var UID: String = ""
    
    init(id: Int, teamA: Team, teamB: Team, date: Date, teamAScore: Int, teamBScore: Int, winner: Team) {
        self.id = id
        self.teamA = teamA
        self.teamB = teamB
        self.date = date
        self.teamAScore = teamAScore
        self.teamBScore = teamBScore
        self.winner = winner
        self.UID = UUID().uuidString
    }
    
    func setTeamAScore(score: Int) {
        self.teamAScore = score
    }
    
    func setTeamBScore(score: Int) {
        self.teamBScore = score
    }
    
    func setWinnerId(team: Team) {
        self.winner = team
    }
    
    func getUID() -> String {
        return UID
    }
    
    func getFavouriteName() -> String {
        return teamA.name + " vs " + teamB.name
    }
    
    
    
}
