import Foundation
import UIKit

class MatchViewController: DetailViewController, PrimarySecondaryPair {
    @IBOutlet weak var teamALogo: UIImageView!
    @IBOutlet weak var teamBLogo: UIImageView!
    @IBOutlet weak var teamAName: UILabel!
    @IBOutlet weak var teamBName: UILabel!
    @IBOutlet weak var teamAScore: UILabel!
    @IBOutlet weak var teamBScore: UILabel!
    @IBOutlet weak var bookmarkButton: UIBarButtonItem!
    @IBOutlet weak var reminder: UITextField!
    
    private let type: String = "match"
    
    private var match: Match!
    private var favourites: Favorites?
    private var favouriteDatabase = FavouriteDatabase.sharedInstance
    
    // Add this match as favourite
    @IBAction func addFavourite() {
        DataSource.singleton.autoEditFavourite(favourite: match, reminder : reminder.text!)
        updateFavourite()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        populateDetails()
        updateFavourite()
    }
    
    func getType() -> String {
        return type
    }
    
    func updateFavourite() {
        let added = DataSource.singleton.favouriteList.filter { $0.getUID() == match.getUID() }.count > 0
        
        //let added = FavouriteDatabase.sharedInstance.favouritedb.filter { $0.favourite_id == match.getUID() }.count > 0
        bookmarkButton.title =  added ? "Remove" : "Add"
    }
    
    // Set match data for this view
    func setMatch(match: Match) {
        self.match = match
        populateDetails()
    }
    
    func populateDetails() {
        if (isViewLoaded && match != nil) {
            teamALogo.image = UIImage (named: match.teamA.logo)
            teamBLogo.image = UIImage (named: match.teamB.logo)
            teamAName.text = match.teamA.name
            teamBName.text = match.teamB.name
            teamAScore.text = String(match.teamAScore)
            teamBScore.text = String(match.teamBScore)
        }
    }
}
