import Foundation
import UIKit

class TeamPlayerListCellView : UITableViewCell{
    @IBOutlet weak var teamPlayerNameView: UILabel!
    
    public func setPlayer(player : String){
        teamPlayerNameView.text = player
    }
}
