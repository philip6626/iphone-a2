import Foundation
import UIKit

class DetailViewController: UIViewController {
    
    // Fade in on appear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.subviews.forEach { subView in
            subView.alpha = 0;
            UIView.animate(withDuration: 0.2 ) {
                subView.alpha = 1;
            }
        }
    }
    
    // Fade out on disappear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.view.subviews.forEach { subView in
            UIView.animate(withDuration: 0.2 ) {
                subView.alpha = 0;
            }
        }
    }
}
