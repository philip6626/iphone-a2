//
//  iphonedevassignmentUITests.swift
//  iphonedevassignmentUITests
//
//  Created by Philip on 22/8/17.
//  Copyright © 2017 swifty. All rights reserved.
//

import XCTest

class iphonedevassignmentUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testGame() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        // run through app from the game selectiong UI until it reaches the team detail page
        // testing each table view have at least one cell
        sleep(5)
        let app = XCUIApplication()
        
        app.buttons["Games"].tap()
        
        /// get first cell
        let firstCell = app.tables.cells.element(boundBy: 0)
        XCTAssert(firstCell.exists)
        firstCell.tap()
        
        // get second cell
        let secondCell = app.tables.cells.element(boundBy: 0)
        XCTAssert(secondCell.exists)
        let textElement2 = secondCell.staticTexts.element
        XCTAssert(textElement2.exists)
        secondCell.tap()
        
        // get third cell
        let thirdCell = app.tables.cells.element(boundBy: 0)
        XCTAssert(thirdCell.exists)
        let textElement3 = thirdCell.staticTexts.element
        XCTAssert(textElement3.exists)
        thirdCell.tap()
    }
    
    
    
    // run through app from the team selectiong UI
    // testing each table view have at least one cell
    func testTeam(){
        sleep(5)
        let app = XCUIApplication()
        
        app.buttons["Teams"].tap()
        
        // get first cell
        let cell = app.tables.cells.element(boundBy: 0)
        XCTAssert(cell.exists)
        let textElement = cell.staticTexts.element
        XCTAssert(textElement.exists)
    }
    
    // test if tapping on team table cell results in correct details view
    func testTeamDetail() {
        sleep(5)
        let app = XCUIApplication()
        
        // go to teams view
        app.buttons["Teams"].tap()
        
        // get first cell
        let cell = app.tables.cells.element(boundBy: 0)
        XCTAssert(cell.exists)
        let textElement = cell.staticTexts.element
        XCTAssert(textElement.exists)
        let text = cell.staticTexts.element.label
        
        // go to detail view
        cell.tap()
        let detail = app.staticTexts[text]
        XCTAssert(detail.exists)
        
    }
    
    // test add favourites
    func testAddFavourite() {
        let app = XCUIApplication()
        sleep(5)
        // go to teams view
        app.buttons["Teams"].tap()
        
        // get first cell
        let cell = app.tables.cells.element(boundBy: 0)
        XCTAssert(cell.exists)
        let textElement = cell.staticTexts.element
        XCTAssert(textElement.exists)
        let text = cell.staticTexts.element.label
        
        // go to detail view
        cell.tap()
        let detail = app.staticTexts[text]
        XCTAssert(detail.exists)

        // tap add favourite
        let favButton = app.buttons["Add"]
        XCTAssert(favButton.exists)
        favButton.tap()
        
        if (app.buttons.matching(identifier: "Teams").count != 0) {
            app.buttons["Teams"].tap()
        }
        
        // go to home
        app.buttons["Home"].tap()
        let favItem = app.staticTexts[text]
        XCTAssert(favItem.exists)
        
    }
    
    func testRemoveFavourite() {
        testAddFavourite()
        
        let app = XCUIApplication()
        
        let cell = app.tables.cells.element(boundBy: 0)
        XCTAssert(cell.exists)
        
        cell.tap()
        
        // remove favourite
        let favButton = app.buttons["Remove"]
        XCTAssert(favButton.exists)
        favButton.tap()
        
        if (app.buttons.matching(identifier: "Back").count != 0) {
            app.buttons["Back"].tap()
        }
    }
}
