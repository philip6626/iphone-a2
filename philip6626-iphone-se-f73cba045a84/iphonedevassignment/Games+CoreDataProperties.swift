//
//  Games+CoreDataProperties.swift
//  iphonedevassignment
//
//  Created by Philip on 19/9/17.
//  Copyright © 2017 swifty. All rights reserved.
//

import Foundation
import CoreData


extension Games {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Games> {
        return NSFetchRequest<Games>(entityName: "Games")
    }

    @NSManaged public var game_id: Int64
    @NSManaged public var game_logo: String?
    @NSManaged public var game_title: String?
    @NSManaged public var tournaments: NSSet?

}

// MARK: Generated accessors for tournaments
extension Games {

    @objc(addTournamentsObject:)
    @NSManaged public func addToTournaments(_ value: Tournaments)

    @objc(removeTournamentsObject:)
    @NSManaged public func removeFromTournaments(_ value: Tournaments)

    @objc(addTournaments:)
    @NSManaged public func addToTournaments(_ values: NSSet)

    @objc(removeTournaments:)
    @NSManaged public func removeFromTournaments(_ values: NSSet)

}
