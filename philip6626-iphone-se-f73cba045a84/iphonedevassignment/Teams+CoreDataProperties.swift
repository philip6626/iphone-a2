//
//  Teams+CoreDataProperties.swift
//  iphonedevassignment
//
//  Created by Philip on 19/9/17.
//  Copyright © 2017 swifty. All rights reserved.
//

import Foundation
import CoreData


extension Teams {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Teams> {
        return NSFetchRequest<Teams>(entityName: "Teams")
    }

    @NSManaged public var team_id: Int64
    @NSManaged public var team_logo: String?
    @NSManaged public var team_name: String?
    @NSManaged public var favourites: Favorites?
    @NSManaged public var matches: Matches?

}
