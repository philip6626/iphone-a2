//
//  Tournaments+CoreDataProperties.swift
//  iphonedevassignment
//
//  Created by Philip on 19/9/17.
//  Copyright © 2017 swifty. All rights reserved.
//

import Foundation
import CoreData


extension Tournaments {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Tournaments> {
        return NSFetchRequest<Tournaments>(entityName: "Tournaments")
    }

    @NSManaged public var tournament_id: Int64
    @NSManaged public var tournament_name: String?
    @NSManaged public var tournament_winner_id: Int64
    @NSManaged public var tournamet_logo: String?
    @NSManaged public var games: Games?
    @NSManaged public var matches: NSSet?

}

// MARK: Generated accessors for matches
extension Tournaments {

    @objc(addMatchesObject:)
    @NSManaged public func addToMatches(_ value: Matches)

    @objc(removeMatchesObject:)
    @NSManaged public func removeFromMatches(_ value: Matches)

    @objc(addMatches:)
    @NSManaged public func addToMatches(_ values: NSSet)

    @objc(removeMatches:)
    @NSManaged public func removeFromMatches(_ values: NSSet)

}
