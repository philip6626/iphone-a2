//
//  Matches+CoreDataProperties.swift
//  iphonedevassignment
//
//  Created by Philip on 19/9/17.
//  Copyright © 2017 swifty. All rights reserved.
//

import Foundation
import CoreData


extension Matches {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Matches> {
        return NSFetchRequest<Matches>(entityName: "Matches")
    }

    @NSManaged public var date: NSDate?
    @NSManaged public var match_id: Int64
    @NSManaged public var match_winner_id: Int64
    @NSManaged public var team_a: Int64
    @NSManaged public var team_a_score: Int64
    @NSManaged public var team_b: Int64
    @NSManaged public var team_b_score: Int64
    @NSManaged public var favourites: Favorites?
    @NSManaged public var teams: NSSet?
    @NSManaged public var tournaments: Tournaments?

}

// MARK: Generated accessors for teams
extension Matches {

    @objc(addTeamsObject:)
    @NSManaged public func addToTeams(_ value: Teams)

    @objc(removeTeamsObject:)
    @NSManaged public func removeFromTeams(_ value: Teams)

    @objc(addTeams:)
    @NSManaged public func addToTeams(_ values: NSSet)

    @objc(removeTeams:)
    @NSManaged public func removeFromTeams(_ values: NSSet)

}
